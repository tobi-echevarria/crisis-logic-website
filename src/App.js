import React from 'react';

import MiniDrawer from './components/MiniDrawer';

function App() {
  return (
    <div className="App">
      <MiniDrawer></MiniDrawer>
    </div>
  );
}

export default App;
